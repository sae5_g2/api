<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: DELETE');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("DELETE FROM ROUTINE WHERE id_routine = ?;");

	if($stm->execute(array($data->id_routine)))
	{
		echo json_encode(array('message' => 'Routine supprimée.'));
	}
	else
	{
		echo json_encode(array('message' => "Une erreur est survenue lors de la suppression de la routine"));
	}
?>