<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("INSERT INTO ETAPE_HISTORY (routine_history, id_etape, date_debut, date_fin, aide, retour) VALUES (?, ?, ?, ?, ?, ?);");

	if($stm->execute(array($data->routine_history, $data->id_etape, $data->date_debut, $data->date_fin, $data->aide, $data->retour)))
	{
		echo json_encode(array("message" => "Instance d'étape crée."));
	}
	else
	{
		echo json_encode(array("message" => "Instance d'étape non crée."));
	}
?>