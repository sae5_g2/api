<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$result = "";

	$id_utilisateur = isset($_GET['id_utilisateur']) ? $_GET['id_utilisateur'] : die();

	include_once("../config.php");
    
    try {

		$stm = $db->prepare("SELECT id_theme, nom, path_image, couleur, rang_liste, id_utilisateur FROM THEME WHERE id_utilisateur = ?;");

        if($stm->execute(array($id_utilisateur)))
        {
            $result = $stm->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($result);
?>