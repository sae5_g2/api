<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("INSERT INTO ETAPE (consigne, path_image, rang_liste, audio_path, id_routine, est_lue) VALUES (?, ?, ?, ?, ?, ?);");

	if($stm->execute(array($data->consigne, $data->path_image, $data->rang_liste, $data->audio_path, $data->id_routine, $data->est_lue)))
	{
		echo json_encode(array('message' => 'Etape crée.'));
	}
	else
	{
		echo json_encode(array('message' => 'Etape non crée.'));
	}
?>