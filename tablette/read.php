<?php
    header('Access-Control-Allow-Origin: *');
    header('Content-Type: application/json');

    $linesContent = "";

    include_once("../config.php");
    
    try {

        $query_result = $db->query("SELECT * FROM TABLETTE;");

        if($query_result != false)
        {
            $linesContent = $query_result->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($linesContent);
?>