<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("INSERT INTO THEME (nom, path_image, couleur, rang_liste, id_utilisateur) VALUES (?, ?, ?, ?, ?);");

	if($stm->execute(array($data->nom, $data->path_image, $data->couleur, $data->rang_liste, $data->id_utilisateur)))
	{
		echo json_encode(array('message' => 'Thème crée.'));
	}
	else
	{
		echo json_encode(array('message' => 'Thème non crée.'));
	}
?>