<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$result = "";

	$id_theme = isset($_GET['id_theme']) ? $_GET['id_theme'] : die();

	include_once("../config.php");
    
    try {

		$stm = $db->prepare("SELECT id_routine, nom, path_image, couleur, rang_liste, id_theme FROM ROUTINE WHERE id_theme = ?;");

        if($stm->execute(array($id_theme)))
        {
            $result = $stm->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($result);
?>