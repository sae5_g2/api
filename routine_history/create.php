<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("INSERT INTO ROUTINE_HISTORY (id_routine) VALUES (?);");

	if($stm->execute(array($data->id_routine)))
	{
		echo json_encode(array('message' => 'Instance de routine crée.'));
	}
	else
	{
		echo json_encode(array('message' => 'Instance de routine non crée.'));
	}
?>