<?php

	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once("../config.php");

	$data = json_decode(file_get_contents("php://input"));

	$stm = $db->prepare("INSERT INTO UTILISATEUR (nom, path_image, couleur, rang_liste, id_tablette) VALUES (?, ?, ?, ?, ?);");

	if($stm->execute(array($data->nom, $data->path_image, $data->couleur, $data->rang_liste, $data->id_tablette)))
	{
		echo json_encode(array('message' => 'Utilisateur crée.'));
	}
	else
	{
		echo json_encode(array('message' => 'Utilisateur non crée.'));
	}
?>