<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$result = "";

	$id_routine = isset($_GET['id_routine']) ? $_GET['id_routine'] : die();

	include_once("../config.php");
    
    try {

		$stm = $db->prepare("SELECT id_etape, consigne, path_image, rang_liste, audio_path, id_routine FROM ETAPE WHERE id_routine = ?;");

        if($stm->execute(array($id_routine)))
        {
            $result = $stm->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($result);
?>