<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$result = "";

	$id_tablette = isset($_GET['id_tablette']) ? $_GET['id_tablette'] : die();

	include_once("../config.php");
    
    try {

		$stm = $db->prepare("SELECT id_utilisateur, nom, path_image, couleur, rang_liste, id_tablette FROM UTILISATEUR WHERE id_tablette = ?;");

        if($stm->execute(array($id_tablette)))
        {
            $result = $stm->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($result);
?>