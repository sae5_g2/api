# API REST en PHP

## Introduction

API Rest développée en PHP. Elle implémente les méthodes CRUD (Create, Read, Update et Delete) pour toutes les tables d'une base de données MySQL. Cette API est nécessaire car on ne peut pas se connecter à la base de données autrement qu'en localhost. Certaines méthodes ne sont pas impémentées car il n'y en a pas l'utilité et si cela était fait, il y aurait des chances que cela comprommette la sécurité des données. A l'heure actuelle (02/2024), l'API et la base de données sont hébergées sur un serveur LAMP (172.30.13.104) disponible uniquement sur le réseau de l'université. Assurez-vous donc d'être soit connectés sur _eduroam_ soit sur _univ-libre-service_ pour y accéder.

## Méthodes CRUD

Les méthodes CRUD sont les suivantes :

| Méthode CRUD | Méthode HTTP | Rôle |
|-----:|---|---|
| Create | POST | Insérer un enregistrement dans une table (__INSERT INTO__) |
| Read | GET | Lire un enregistrement spécifique en précisant son id, une liste d'enregistrements en fonction d'un critère ou tous les enregistrements d'une table (__SELECT__)
| Update | PUT | Modifier les valeurs des colonnes d'un enregistrement (__UPDATE__) |
| Delete | DELETE | Supprimer un enregistrement d'une table en spécifiant son id (__DELETE FROM__)

## Arborescence

l'arborescence du projet est la suivante :

_nom_de_table_/_methode_crud.php_

## Tests de l'API

Pour tester et utiliser l'API, vous devrez télécharger et installer Postman (https://www.postman.com/)

1. Une fois téléchargé ouvrez-le
2. Connectez-vous ou créez un compte puis connectez-vous
3. Cliquez sur le bouton _Import_ en haut à gauche de l'écran
4. Téléchargez le fichier *Tests.postman_collection.json* situé à la racine de ce repository
5. Importez les tests en drag and drop le fichier téléchargé dans postman

## Idées d'améliorations

- Token de vérification pour sécuriser l'accès à l'API
- Ajout de nouvelles réponses de l'API lorsqu'une contrainte de la base de données est violée lors d'une insertion par exemple (Dans l'état actuel même si une contrainte est violée l'utilisateur n'est pas informé que l'insertion n'a pas eu lieu car le message est par exemple : "Un thème a été créer")
