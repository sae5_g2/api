<?php

	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	$result = "";

	$routine_history = isset($_GET['routine_history']) ? $_GET['routine_history'] : die();

	include_once("../config.php");
    
    try {

		$stm = $db->prepare("SELECT ETAPE_HISTORY.routine_history, id_etape, date_debut, date_fin, aide, retour, id_routine FROM ETAPE_HISTORY JOIN ROUTINE_HISTORY ON ROUTINE_HISTORY.routine_history = ETAPE_HISTORY.routine_history WHERE ETAPE_HISTORY.routine_history = ?;");

        if($stm->execute(array($routine_history)))
        {
            $result = $stm->fetchAll();
        }

    } catch(PDOException $e) {
        $e->getMessage();
    }

    echo json_encode($result);
?>